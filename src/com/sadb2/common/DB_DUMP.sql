/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lashan
 * Created: Nov 10, 2021
 */

/*
 Navicat Premium Data Transfer

 Source Server         : Local_3307
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3307
 Source Schema         : sadb2

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 10/11/2021 23:11:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `nic` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fname` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lname` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nic`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES ('895506265V', '0715566332', 'Ruwan', 'Tharaka', 'Male');
INSERT INTO `customer` VALUES ('900592036V', '0715536996', 'Hasitha', 'Perera', 'Male');
INSERT INTO `customer` VALUES ('C001', '0332225588', 'Cash', 'Customer', 'Male');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `empid` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fname` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lname` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contactno` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isactive` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`empid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('E001', 'Nuwan', 'Pradeep', '0112235985', 1);
INSERT INTO `employee` VALUES ('E002', 'Ruwan', 'Pathirana', '0222778888', 1);
INSERT INTO `employee` VALUES ('E003', 'Sahan', 'Dammika', '033225623', 1);
INSERT INTO `employee` VALUES ('E004', 'Tharaka', 'Supun', '011225588', 0);
INSERT INTO `employee` VALUES ('E005', 'Kasun', 'Nishantha', '1122', 1);
INSERT INTO `employee` VALUES ('E006', 'lashan', 'chandika', '112', 1);

-- ----------------------------
-- Table structure for invoice
-- ----------------------------
DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice`  (
  `invoiceno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `customer` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  `total` double NULL DEFAULT NULL,
  `discount` double NULL DEFAULT NULL,
  `nettotal` double NULL DEFAULT NULL,
  `payment` double NULL DEFAULT NULL,
  `employee` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`invoiceno`) USING BTREE,
  INDEX `FK_CUSTOMER`(`customer`) USING BTREE,
  INDEX `FK_SADB2_EMPLOYEE`(`employee`) USING BTREE,
  CONSTRAINT `FK_CUSTOMER` FOREIGN KEY (`customer`) REFERENCES `customer` (`nic`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_SADB2_EMPLOYEE` FOREIGN KEY (`employee`) REFERENCES `employee` (`empid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invoice
-- ----------------------------
INSERT INTO `invoice` VALUES ('INV1', 'C001', '2021-09-27 23:25:18', 2255, 5, 2250, 3000, 'E005');
INSERT INTO `invoice` VALUES ('INV2', 'C001', '2021-09-27 23:28:23', 1480, 20, 1460, 1500, 'E005');
INSERT INTO `invoice` VALUES ('INV3', 'C001', '2021-11-05 20:58:19', 6065, 65, 6000, 10000, 'E005');

-- ----------------------------
-- Table structure for invoiceitem
-- ----------------------------
DROP TABLE IF EXISTS `invoiceitem`;
CREATE TABLE `invoiceitem`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `unitprice` double NULL DEFAULT NULL,
  `qty` double NULL DEFAULT NULL,
  `total` double NULL DEFAULT NULL,
  `discount` double NULL DEFAULT NULL,
  `subtotal` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_INVOICE`(`invoiceno`) USING BTREE,
  INDEX `FK_ITEMCODE`(`code`) USING BTREE,
  CONSTRAINT `FK_INVOICE` FOREIGN KEY (`invoiceno`) REFERENCES `invoice` (`invoiceno`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_ITEMCODE` FOREIGN KEY (`code`) REFERENCES `item` (`code`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invoiceitem
-- ----------------------------
INSERT INTO `invoiceitem` VALUES (1, 'INV2', 'I001', 200, 1, 200, 0, 200);
INSERT INTO `invoiceitem` VALUES (2, 'INV2', 'I002', 100, 4, 400, 5, 380);
INSERT INTO `invoiceitem` VALUES (3, 'INV2', 'I005', 450, 2, 900, 0, 900);
INSERT INTO `invoiceitem` VALUES (4, 'INV3', 'I001', 200, 2, 400, 0, 400);
INSERT INTO `invoiceitem` VALUES (5, 'INV3', 'I003', 250, 1, 250, 0, 250);
INSERT INTO `invoiceitem` VALUES (6, 'INV3', 'I004', 60, 4, 240, 0, 240);
INSERT INTO `invoiceitem` VALUES (7, 'INV3', 'I005', 450, 10, 4500, 5, 4275);
INSERT INTO `invoiceitem` VALUES (8, 'INV3', 'I006', 180, 5, 900, 0, 900);

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item`  (
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qty` double NULL DEFAULT NULL,
  `unitprice` double NULL DEFAULT NULL,
  `imgpath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item
-- ----------------------------
INSERT INTO `item` VALUES ('I001', 'Bread', 83, 200, 'H:/Project_Dir/fooditem/bread.jpeg', 1);
INSERT INTO `item` VALUES ('I002', 'Biscut', 100, 100, 'H:/Project_Dir/fooditem/biscut.jpeg', 1);
INSERT INTO `item` VALUES ('I003', 'Chicken Bun', 159, 250, 'H:/Project_Dir/fooditem/chickenbun.jpeg', 1);
INSERT INTO `item` VALUES ('I004', 'Coke and Soft Drinks', 216, 60, 'H:/Project_Dir/fooditem/coke.jpeg', 1);
INSERT INTO `item` VALUES ('I005', 'Macoroni', 70, 450, 'H:/Project_Dir/fooditem/macoroni.jpeg', 1);
INSERT INTO `item` VALUES ('I006', 'Water Melon Juice', 95, 180, 'H:/Project_Dir/fooditem/watermelomjuice.jpeg', 1);
INSERT INTO `item` VALUES ('I007', 'Mojito', 100, 260, 'H:/Project_Dir/fooditem/mojitococktail.jpg.jpg', 1);

-- ----------------------------
-- Table structure for systemuser
-- ----------------------------
DROP TABLE IF EXISTS `systemuser`;
CREATE TABLE `systemuser`  (
  `username` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `employee` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `usertype` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isactive` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`username`) USING BTREE,
  INDEX `FK_EMPLOYEE_idx`(`employee`) USING BTREE,
  INDEX `FK_USERTYPE_idx`(`usertype`) USING BTREE,
  CONSTRAINT `FK_EMPLOYEE` FOREIGN KEY (`employee`) REFERENCES `employee` (`empid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_USERTYPE` FOREIGN KEY (`usertype`) REFERENCES `usertype` (`code`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of systemuser
-- ----------------------------
INSERT INTO `systemuser` VALUES ('admin', '202cb962ac59075b964b07152d234b70', 'E005', 'ADM', 1);
INSERT INTO `systemuser` VALUES ('kasun', '202cb962ac59075b964b07152d234b70', 'E005', 'USR', 1);
INSERT INTO `systemuser` VALUES ('lashan', '123', 'E006', 'ADM', 1);
INSERT INTO `systemuser` VALUES ('nuwan', 'abc', 'E001', 'USR', 1);

-- ----------------------------
-- Table structure for usertype
-- ----------------------------
DROP TABLE IF EXISTS `usertype`;
CREATE TABLE `usertype`  (
  `code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usertype
-- ----------------------------
INSERT INTO `usertype` VALUES ('ADM', 'Admin');
INSERT INTO `usertype` VALUES ('USR', 'User');

SET FOREIGN_KEY_CHECKS = 1;
