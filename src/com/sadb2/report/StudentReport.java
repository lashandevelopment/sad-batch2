/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sadb2.report;

import com.sadb2.common.SystemVariableList;
import com.sadb2.model.StudentDto;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Lashan
 */
public class StudentReport {
    public static void main(String[] args) {
        try {
            StudentDto s1 = new StudentDto();
            s1.setStudent_id("S001");
            s1.setFirst_name("Sadun");
            s1.setLast_name("Perera");
            s1.setAge(25);
            
            StudentDto s2 = new StudentDto();
            s2.setStudent_id("S002");
            s2.setFirst_name("Ruwan");
            s2.setLast_name("Gamage");
            s2.setAge(36);
            
            List<StudentDto> student_list = new ArrayList<>();
            student_list.add(s1);
            student_list.add(s2);
            
            String path = SystemVariableList.REPORT_BASE_URL + "studentbeandata.jasper";
            InputStream inputStream = new FileInputStream(path);
            
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(student_list);
            
            JasperPrint fillReport = JasperFillManager.fillReport(inputStream, null, dataSource);
            JasperViewer.viewReport(fillReport, false);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
