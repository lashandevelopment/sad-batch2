/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sadb2;

import com.sadb2.dto.WorkerDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Lashan
 */
public class ABC {

    public static void main(String[] args) {

        List<String> list2 = new ArrayList();
        list2.add("Hashan");
        list2.add("Nuwan");
        list2.add("Saman");
        list2.add("Hasun");
        list2.add("Dasun");

        for (String name : list2) {
            if (name.substring(0, 1).equals("H")) {
                System.out.println(name);
            }
        }

        list2.stream().forEach(name -> System.out.println(name));

        list2.stream().forEach(name -> {
            if (name.substring(0, 1).equals("H")) {
                System.out.println(name);
            }
        });

        list2.stream()
                .filter(name -> name.startsWith("H"))
                .forEach(name -> System.out.println(name));

    }

}
